<div align="center">
<h1>LOGBOOK 
</div>

# Week 1 : 
  After a quick consultation of our team members, it appeared that nobody knows about Godot or it's concepts

  To solve this probleme, we spend our time to look documentation abouth the languague as well as it's concepts (node and scenes oriented) then differences between its versions (Godot 2,3 and 4)

## Week 2 goal : 
  Continue to read documentation about Godot3, Godot 4 and differences between theses versions and Godot2

---

# Week 2 :
  Continue to learn about Godot2 and Godot3

  We inquire about automatic conversion from Godot2 to Godot3, it seems it does not work well and is still in progress while Godot 4 already has Beta realease. We concluded the automatic conversion will never be finished and so is not a workable solution

  After investigation, we have to transcribe all .xscn files into .tscn files (93 files, 22k+ lines) and even after using the automatic conversion .gd files (GodotScript) does not seems to be totaly or well transcribed (25k+ line in all .gd files, estimated 10% to correct)

  4 possibilities are avaibles : 

  - Migrating from Godot2 to Godot3 : 
    Knowing the automatic translation from Godot3 to Godot4 seems to work well (and will probably be updated with futures updates of Godot4) migrate to Godot4 without much troubles. Futhermore, the conversion Godot2 to Godot3 chews up part of the work to achieve the transfer.

- Migrating from Godot3 to Godot4 :
    Seems unpromising because Godot4 is not finished and the documentation is not yet up to date and there is nothing existing between Godot2 and Godot4 to convert unlike Godot2 to Godot3.

- Creating a Godot2 to Godot3 parser :
    After documentation, this doesn't seem to be a light option and would require a lot of time, moreover there doesn't seem to be a simple method to go from one to the other, implying that one would have to understand in detail the nuances and equivalences between Godot2 and Godot3
  
- Recreating the game on Godot3 or Godot4 from scratch :
    Not really interesting idea because the parts to be modified to make the port, although not negligible, are far from representing the whole existing project.
  

  So we start with the idea of transferring Godot2 to Godot3 without making a parser, modifying what is necessary by hand.
  
## Week 3 goal :
 
  Transcribe some scenes from Godot2 to Godot3
  
---

# Week 3 : 
  After discussion with the teacher (Mr Palix) the alternative of a parser to minina to do the biggest part of transcription from godot2 to godot3 was considered \
  This being, before making any operation, we concentrated to find and understand the main differences between Godot2 and Godot3 by practicing the transcription between the 2 as well as creating similar scenes on both to find the differences and similarities
  
  A possibility to easily convert .xscn to .tscn seems to exist, which would reduce considerably the time spent to make the conversion.
  
  We have done the migration from .xscn to .tscn without any problem (staying in godot2)\
  We also fixed the few dependency errors that occurred so that the game in godot2 works without problem with only the tscn
  
## Week 4 goal : 
  Continue on this path to see if other ways exist to transcribe the Godot2 formats to Godot 3
  
---

# Week 4 : 
  This week we discovered and made the transition from .xml to .tres (while staying in godot2) (indeed the xml in GD2 are the equivalent of .tres which are also used in GD3)
  
  After these transormations made, we launched the existing exporter of GD2 towards GD3 to take a load the biggest part of the modification of the .tres and the .tscn
  
  The rest of the session was focused on the correction of errors in .gd, .tscn and .tres in GD3\
  You will find more information in the file "G2toG3.md" for what concerns the correction of these errors
  
## Week 5 goal :
  Continue to find and solve these bugs 

---

# Week 5 : 
  We continued to solve bugs (that you could find detailed in the file "G2toG3.md" as before)\
  We also found two files "scripts/tutorial_message.gd" and "scripts/tutorial.gd" that are not important for the game execution, probably the initial creators had as project to create a tutorial that was aborted and replaced by the campaign, they must have forgotten to delete them from the game files, indeed there is no other trace of tutorial in everything we saw, neither in the source code nor in the graphical interfaces
  
## Week 6 goal : 
  Continue the research and correction of bugs in Godot3  

---

# Week 6 : 
  This week we have used our time to translate all documentation from french to English \
  we also spend time thinking about what we did for the past 5 weeks. What we did wrong, what we did right, reasons we became more efficient, and making a clear point about our organization
  
  here are the results of our retrospective : 
  - the more time passes, the more precise the commits names
  - our pair programming/working is efficient allowing us to have a really low error rate and quickly find how to correct bugs (furthermore knowing that bugs are found one by one this work pattern  excludes the possibility of having someone with nothing to do)
  - being two teams helps us have more precise tasks and be faster while excluding working on the exact same thing by accident : 
    - one team focuses on finding pending bugs and how to resolve them
    - while the other one finds all other occurrences of this bug and corrects them all
    
  we also spend time preparing for a quick presentation of the advancements in our project
  
## Week 7 goal : 
  going back to hunt bugs down and correct them
  
---

# Week 7 : 
  we continue to hunt bugs and correct them
  
  A big bug occurred with the audio in Godot3. The way audio work in Godot2 and Godot3 is drastically different (the sound engine changed) so further investigation is necessary\
  for now, we think that we will have to recreate how audio work for the whole game. Indeed, we are reading documentation and will write one when this is over
  
  After research we found of to patch this, more information are in G2toG3.md or in issue #1
  
## Week 8 goal : 
  We will continue, as previus weeks to look after and correct bugs

---

# Week 8 : 
  We created an issue on the original git of Tanks Of Freedom about what seems to be useless files\
  An answer was given to us, it seems we were right, these files are probably useless, but from the answer we had, these are not the only useless files or lines of code in the project. \
  In fact, it is such a mess it's no longer maintained for several years, and Tanks Of Freedom 2 is a whole other project because of the current stage of Tanks Of Freedom
  
  we also continue to hunt bugs down, we finally progress enough to begin to solve UI bugs and not just initialization ones.\
  This means we have to look for bugs and they will not anymore jump on us. this allows us to simulate and so solve more bugs at the same time
  to adapt to this new environment, we changed our work organization.
  
  - we kept on creating an issue for every bug we see and commit only one resolution at a time.
  - we quit the pair programming method, from now on, everyone takes an issue, and resolves it, if there is no issue left he tries to find new bugs and report them as issues

  This being said, we still have some pair programming going on but only for big bugs, like the map not showing

## Week 9 goal : 

  As always we will continue to look after and correct bugs we found

---

# Week 9 : 
  the bug hunting is still going, we found a lot of bugs but corrected as much. We can see our advancement with the game being more and more usable. \
  We unlock new interactions, reveal new bugs, and so on.
  
## week 10 goal : 
  same as always but we will particularlly focus on bugs on the game, for exemples : deplacements, capture, attack ...
  
---

# Week 10 : 
  as for the previus weeks we continue tu hunt down bugs but soon realised we will not be able to fix them all.
  For this reason we chossed to list all things we achived and what still need to be done. \
  Quicly here are the things we achived :
  - we can launch the game 
  - missions and their cinematics are working smooth
  - maps are working
  - AI can play against each other and agains the player
  - settings work perfectly fine
  - sounds effect and music sound good

  We also resumed our organisation. \
  All this informations can be found in the Final report named : "Report.pdf" 
